#pragma once

#include "Movil.h"

class MovilBlando : public Movil
{
public:
	MovilBlando(void);
	virtual ~MovilBlando(void);

	virtual void simular(double inc_t); 
};

